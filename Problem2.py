import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas as pd
import numpy as np

app = dash.Dash(__name__)
server = app.server

airports = pd.read_csv("https://gitlab.com/nichensh/my-project/-/raw/main/airports.csv")
flights = pd.read_csv("https://gitlab.com/nichensh/my-project/-/raw/main/flights.csv")

flight_data = flights
flight_data.rename(columns = {'dest':'faa'},inplace=True)
flight_data = pd.merge(flight_data,airports,how='inner',on='faa')
flight_data = pd.DataFrame({'state':np.sort(flight_data.state.unique()),'flight count':np.array(flight_data.value_counts('state',sort=False))})
my_figure = px.choropleth(flight_data, locations='state',locationmode='USA-states',color='flight count',scope='usa',labels={'flight count':'flight count'})

weather = pd.read_csv("https://gitlab.com/nichensh/my-project/-/raw/main/weather.csv")

mean_wind_speed_EWR = weather.groupby('month')['wind_speed_EWR'].mean()
mean_wind_speed_JFK = weather.groupby('month')['wind_speed_JFK'].mean()
mean_wind_speed_LGA = weather.groupby('month')['wind_speed_LGA'].mean()
mean_wind_speed_EWR = pd.DataFrame(mean_wind_speed_EWR)
mean_wind_speed_EWR=mean_wind_speed_EWR.rename(columns={'wind_speed_EWR':'EWR'})
figure_test = px.line(mean_wind_speed_EWR,title='Variation of mean of wind speed by month in 2013')
figure_test.update_layout(yaxis_title='mean of wind speed')
wind_data_JFK = {'month':[1,2,3,4,5,6,7,8,9,10,11,12],'mean_wind_speed_JFK':np.array(mean_wind_speed_JFK)}
wind_data_JFK=pd.DataFrame(wind_data_JFK)
wind_data_LGA = {'month':[1,2,3,4,5,6,7,8,9,10,11,12],'mean_wind_speed_LGA':np.array(mean_wind_speed_LGA)}
wind_data_LGA = pd.DataFrame(wind_data_LGA)
figure_test.add_scatter(x=wind_data_JFK['month'],y=wind_data_JFK['mean_wind_speed_JFK'],mode='lines',name='JFK')
figure_test.add_scatter(x=wind_data_LGA['month'],y=wind_data_LGA['mean_wind_speed_LGA'],mode='lines',name='LGA')

mean_humid_EWR = weather.groupby('month')['humid_EWR'].mean()
mean_humid_JFK = weather.groupby('month')['humid_JFK'].mean()
mean_humid_LGA = weather.groupby('month')['humid_LGA'].mean()
mean_humid_EWR = pd.DataFrame(mean_humid_EWR)
mean_humid_EWR = mean_humid_EWR.rename(columns={'humid_EWR':'EWR'})
figure_test1 = px.line(mean_humid_EWR,title='Variation of mean humid level by month in EWR, JFK and LGA in 2013')
figure_test1.update_layout(yaxis_title='mean of humid level')
humid_data_JFK = {'month':[1,2,3,4,5,6,7,8,9,10,11,12],'JFK':np.array(mean_humid_JFK)}
humid_data_JFK = pd.DataFrame(humid_data_JFK)
humid_data_LGA = {'month':[1,2,3,4,5,6,7,8,9,10,11,12],'LGA':np.array(mean_humid_LGA)}
humid_data_LGA = pd.DataFrame(humid_data_LGA)
figure_test1.add_scatter(x=humid_data_JFK['month'],y=humid_data_JFK['JFK'],mode='lines',name='JFK')
figure_test1.add_scatter(x=humid_data_LGA['month'],y=humid_data_LGA['LGA'],mode='lines',name='LGA')

app.layout = html.Div(children=[
    html.H1(children='Problem 2'),

    html.Div(children='''
        Problem2(a)
    '''),

    dcc.Graph(
        id='my-graph',
        figure=my_figure
    ),
    
    html.Div(children='''
        Problem2(b)
    '''),
    
    dcc.Graph(
        id = 'figure_test',
        figure=figure_test
    ),
    
    html.Div(children='''
        From the above line chart we can see that in 2013, JFK had the largest mean wind speed every month, followed by LGA and EWR. The three regions all have a relatively high wind speed in spring and winter, and a relatively low wind speed in summer.
    '''),
    
    dcc.Graph(
        id = 'figure_test1',
        figure = figure_test1
    ),

    html.Div(children='''
        From the above line chart we can see that in 2013, EWR and JFK had the similar humidity level in spring and winter, while JFK had the highest humidity level in summer among the three regions. The humidity level of JFK and EWR fluctuates greatly throughout the year, while that of LGA is relatively stable.
    ''')
])

if __name__ == '__main__':
    app.run_server(debug=True, port=8080)